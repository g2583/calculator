package com.example.calculator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    public TextField input;
    @FXML
    public Label disp;
    @FXML
    public TextField err;
    @FXML
    public Button one;
    @FXML
    public Button two;
    @FXML
    public Button three;
    @FXML
    public Button four;
    @FXML
    public Button five;
    @FXML
    public Button six;
    @FXML
    public Button seven;
    @FXML
    public Button eight;
    @FXML
    public Button nine;
    @FXML
    public Button zero;
    @FXML
    public Button plus;
    @FXML
    public Button minus;
    @FXML
    public Button multiply;
    @FXML
    public Button divide;
    @FXML
    public Button clear;
    @FXML
    public Button clearAll;
    @FXML
    public Button equals;

    private long num;
    private long num2;
    private String operator;

    public void one_click(){
        String val = input.getText();
        String set = "1";
        input.setText(val + set);
    }

    public void two_click(){
        String val = input.getText();
        String set = "2";
        input.setText(val + set);
    }

    public void three_click(){
        String val = input.getText();
        String set = "3";
        input.setText(val + set);
    }

    public void four_click(){
        String val = input.getText();
        String set = "4";
        input.setText(val + set);
    }

    public void five_click(){
        String val = input.getText();
        String set = "5";
        input.setText(val + set);
    }

    public void six_click(){
        String val = input.getText();
        String set = "6";
        input.setText(val + set);
    }

    public void seven_click(){
        String val = input.getText();
        String set = "7";
        input.setText(val + set);
    }

    public void eight_click(){
        String val = input.getText();
        String set = "8";
        input.setText(val + set);
    }

    public void nine_click(){
        String val = input.getText();
        String set = "9";
        input.setText(val + set);
    }

    public void zero_click(){
        String val = input.getText();
        String set = "0";
        input.setText(val + set);
    }

    public void multiply_click(){
        String val = input.getText();
        if(!val.isEmpty()) {
            err.setText("");
            this.num = Integer.parseInt(val);
            input.setText("");
            disp.setText(this.num + " * ");
            operator = "*";
        }else{
            err.setText("Input Empty Error");
        }
    }

    public void add_click(){
        String val = input.getText();
        if(!val.isEmpty()) {
            err.setText("");
            this.num = Integer.parseInt(val);
            input.setText("");
            disp.setText(val + " + ");
            operator = "+";
        }else{
            err.setText("Input Empty Error");
        }
    }

    public void minus_click(){
        String val = input.getText();
        if(!val.isEmpty()) {
            err.setText("");
            this.num = Integer.parseInt(val);
            input.setText("");
            disp.setText(this.num + " - ");
            operator = "-";
        }else{
            err.setText("Input Empty Error");
        }
    }

    public void divide_click(){
        String val = input.getText();
        if(!val.isEmpty()) {
            err.setText("");
            this.num = Integer.parseInt(val);
            input.setText("");
            disp.setText(this.num + " / ");
            operator = "/";
        }else{
            err.setText("Input Empty Error");
        }
    }

    public void clear_click(){
        input.setText("");
    }

    public void clearAll_click(){
        input.setText("");
        disp.setText("");
        err.setText("");
        num =0;
        num2 =0 ;
    }

    public void equals_click(){
        if(input.getText().isBlank() || disp.getText().isEmpty()){
            System.out.println("THE INPUT IS BLANK!");
            err.setText("Input Empty Error");
        }else {
            err.setText("");
            switch (operator) {
                case "+" -> {
                    String val = input.getText();
                    this.num2 = Integer.parseInt(val);
                    long ret = this.num + this.num2;
                    input.setText(String.valueOf(ret));
                    String k = disp.getText();
                    disp.setText(k + val);
                }
                case "*" -> {
                    String Mval = input.getText();
                    this.num2 = Integer.parseInt(Mval);
                    long Mret = this.num * this.num2;
                    input.setText(String.valueOf(Mret));
                    String m = disp.getText();
                    disp.setText(m + Mval);
                }
                case "-" -> {
                    String Sval = input.getText();
                    this.num2 = Integer.parseInt(Sval);
                    long Sret = this.num - this.num2;
                    input.setText(String.valueOf(Sret));
                    String s = disp.getText();
                    disp.setText(s + Sval);
                }
                case "/" -> {
                    String Dval = input.getText();
                    this.num2 = Integer.parseInt(Dval);
                    long Dret = this.num / this.num2;
                    input.setText(String.valueOf(Dret));
                    String d = disp.getText();
                    disp.setText(d + Dval);
                }
            }
        }
    }
}
